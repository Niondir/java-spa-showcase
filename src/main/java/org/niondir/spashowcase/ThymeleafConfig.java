package org.niondir.spashowcase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

/**
 * Created by Tarion on 23.12.2014.
 */
@Configuration
public class ThymeleafConfig {

	@Bean(name = "defaultTemplateResolver")
	ServletContextTemplateResolver defaultTemplateResolver() {
		return new ServletContextTemplateResolver() {{
			setPrefix("/WEB-INF/templates/");
			setSuffix(".html");
			setTemplateMode("HTML5");
		}};
	}

	@Bean(name = "templateEngine")
	SpringTemplateEngine templateEngine() {
		return new SpringTemplateEngine() {{
			setTemplateResolver(defaultTemplateResolver());
		}};
	}

	@Bean(name = "viewResolver")
	ViewResolver viewResolver() {
		return new ThymeleafViewResolver() {{
			setTemplateEngine(templateEngine());
			setOrder(1);
			setViewNames(new String[]{"*.html", "*.xhtml"});
		}};
	}

}
