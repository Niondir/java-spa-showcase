package org.niondir.spashowcase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Tarion on 23.12.2014.
 */
public class Application {
	

	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(AppConfig.class);
		app.setShowBanner(true);
		app.run(args);
	}
}
