package org.niondir.spashowcase;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.script.ScriptEngineManager;

/**
 * Created by Tarion on 23.12.2014.
 */
@Configuration
//@ImportResource("classpath:applicationContext.xml")
@EnableAutoConfiguration
@ComponentScan
public class AppConfig {
	
	@Bean
	ScriptEngineManager scriptEngineManager() {
		return new ScriptEngineManager();
		
	}
	
}
