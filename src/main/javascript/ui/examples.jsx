/**
 * @jsx React.DOM
 */

var React = require('../bower_components/react/react.js');

var SimpleProperty = React.createClass({
	render: function () {
		return <div>
			<h1>String from props</h1>
			<p>{this.props.msg}</p>
		</div>
	}
});


exports.SimpleProperty = SimpleProperty;

var FromModel = React.createClass({
	/* This will break at runtime:
	getInitialState: function () {
		return {msg: 'loading ...'};
	}, */
	/*
	 componentDidMount: function () {
	 //this.setState({msg: 'success!'});
	 },*/
	render: function () {
		return (
				<div>
					<h1>String from model</h1>
					<p>BROKEN! :(</p>
				</div>
				);
	}
});

exports.FromModel = FromModel;