/** This is the server side entry point **/

/* nashorn compatibility */
console = {
	log: print,
	warn: print,
	error: print
};

// React expects 'window' or 'global' to be set
var global = this;

var shared = require('./shared');
var React = require('./bower_components/react/react.js');


var page = React.createFactory(shared.page);


function renderServerside() {
	return React.renderToString(page({msg: 'World!'}));
}

exports.renderServerside = renderServerside;