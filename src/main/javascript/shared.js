/* This is the main entry point, shared by client and server*/

var React = require('./bower_components/react/react.js');

var examples = require('./ui/examples.jsx');

var SimpleProperty = React.createFactory(examples.SimpleProperty);
var FromModel = React.createFactory(examples.FromModel);

var MyComponent = React.createClass({
	render: function () {
		return React.DOM.div(null,
				SimpleProperty({msg: 'success!'})
				, FromModel(null)
		)
	}
});

exports.page = MyComponent;